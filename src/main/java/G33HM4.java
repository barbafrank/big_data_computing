import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaDoubleRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Arrays;

public class G33HM4 {
    public static void main(String[] args) throws Exception {
        //------- PARSING CMD LINE ------------
        // Parameters are:
        // <path to file>, k, L and iter
        if (args.length != 4) {
            System.err.println("USAGE: <filepath> k L iter");
            System.exit(1);
        }
        String inputPath = args[0];
        int k = 0, L = 0, iter = 0;
        try {
            k = Integer.parseInt(args[1]);
            L = Integer.parseInt(args[2]);
            iter = Integer.parseInt(args[3]);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        if(k <= 2 && L <= 1 && iter <= 0) {
            System.err.println("Something wrong here...!");
            System.exit(1);
        }

        //------------------------------------
        final int k_fin = k;

        //------- DISABLE LOG MESSAGES
        Logger.getLogger("org").setLevel(Level.OFF);
        Logger.getLogger("akka").setLevel(Level.OFF);

        //------- SETTING THE SPARK CONTEXT
        SparkConf conf = new SparkConf(true).setAppName("kmedian new approach");
        JavaSparkContext sc = new JavaSparkContext(conf);

        //------- PARSING INPUT FILE ------------
        JavaRDD<Vector> pointset = sc.textFile(args[0], L)
                .map(x-> strToVector(x))
                .repartition(L)     // this repartition shouldn't be needed, since we
                                    // already load the text file in L partitions
                .cache();
        long N = pointset.count();
        System.out.println("\nNumber of points is: " + N);
        System.out.println("Number of clusters is: " + k);
        System.out.println("Number of parts is: " + L);
        System.out.println("Number of iterations is: " + iter);

        //------- SOLVING THE PROBLEM ------------
        double obj = MR_kmedian(pointset, k, L, iter);
        System.out.println("\nObjective function is: <" + obj + ">");
        
    }

    public static Double MR_kmedian(JavaRDD<Vector> pointset, int k, int L, int iter) {
        long start = System.currentTimeMillis();

        //------------- ROUND 1 ---------------------------
        JavaRDD<Tuple2<Vector,Long>> coreset = pointset.mapPartitions(x ->
        {
            ArrayList<Vector> points = new ArrayList<>();
            ArrayList<Long> weights = new ArrayList<>();
            while (x.hasNext()) {
                points.add(x.next());
                weights.add(1L);
            }
            ArrayList<Vector> centers = Kmedian.kmeansPP(points, weights, k, iter);
            ArrayList<Long> weight_centers = compute_weights(points, centers);
            ArrayList<Tuple2<Vector,Long>> c_w = new ArrayList<>();
            for(int i = 0; i < centers.size(); ++i) {
                Tuple2<Vector, Long> entry = new Tuple2<>(centers.get(i), weight_centers.get(i));
                c_w.add(i,entry);
            }
            return c_w.iterator();
        });

        //------------- ROUND 2 ---------------------------
        ArrayList<Tuple2<Vector, Long>> elems = new ArrayList<>(k*L);

        coreset.cache();
        elems.addAll(coreset.collect());

        System.out.println("Round 1 done in: " + (System.currentTimeMillis() - start) + " [ms]");
        start = System.currentTimeMillis();

        ArrayList<Vector> coresetPoints = new ArrayList<>(k * L);
        ArrayList<Long> weights = new ArrayList<>(k * L);

        for(Tuple2<Vector, Long> elem : elems) {
            coresetPoints.add(elem._1);
            weights.add(elem._2);
        }

        ArrayList<Vector> centers = Kmedian.kmeansPP(coresetPoints, weights, k, iter);

        System.out.println("Round 2 done in: " + (System.currentTimeMillis() - start) + " [ms]");
        start = System.currentTimeMillis();

        //------------- ROUND 3: COMPUTE OBJ FUNCTION --------------------
        //compute a JavaDoubleRDD of distances that maps each point of the pointset
        //to the distance between it and its closest center
        JavaDoubleRDD distances = pointset.mapPartitionsToDouble(x -> {
            ArrayList<Double> dists = new ArrayList<>();
            double min_dist, curr_dist;
            Vector curr_vec;
            while (x.hasNext()) {
                curr_vec = x.next();
                min_dist = Double.POSITIVE_INFINITY;
                for(Vector center : centers)
                    if((curr_dist = Vectors.sqdist(center, curr_vec)) < min_dist)
                        min_dist = curr_dist;
                dists.add(Math.sqrt(min_dist));
            }
            return dists.iterator();
        });

        //Compute the mean (i.e. the objective function we are considering) and return it
        double mean = distances.mean();
        System.out.println("Round 3 done in: " + (System.currentTimeMillis() - start) + " [ms]");
        return mean;
    }

    public static ArrayList<Long> compute_weights(ArrayList<Vector> points, ArrayList<Vector> centers) {
        Long weights[] = new Long[centers.size()];
        Arrays.fill(weights, 0L);
        for(int i =0; i < points.size(); ++i) {
            double tmp = euclidean(points.get(i), centers.get(0));
            int mycenter = 0;
            for(int j = 1; j < centers.size(); ++j) {
                if(euclidean(points.get(i),centers.get(j)) < tmp) {
                    mycenter = j;
                    tmp = euclidean(points.get(i), centers.get(j));
                }
            }
            weights[mycenter] += 1L;
        }
        ArrayList<Long> fin_weights = new ArrayList<>(Arrays.asList(weights));
        return fin_weights;
    }

    public static Vector strToVector(String str) {
        String[] tokens = str.split(" ");
        double[] data = new double[tokens.length];
        for (int i = 0; i < tokens.length; i++) {
            data[i] = Double.parseDouble(tokens[i]);
        }
        return Vectors.dense(data);
    }

    // Euclidean distance
    public static double euclidean(Vector a, Vector b) {
        return Math.sqrt(Vectors.sqdist(a, b));
    }
}