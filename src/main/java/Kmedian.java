import org.apache.spark.mllib.linalg.BLAS;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import scala.Tuple2;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Random;

import static java.lang.Integer.parseInt;

public class Kmedian {

    // main method
    public static void main(String[] args) throws IOException {
        // check for the presence of all the required command line arguments
        if (args.length != 3) {
            throw new IllegalArgumentException("Bad command line arguments," +
                    " the program expects 'datafile num_clusters max_iterations'" +
                    " in this order");
        }

        // load parameters as constants
        final int k = parseInt(args[1]);
        final int iter = parseInt(args[2]);

        // import the array of vectors using the
        // provided method, vectors represent
        // points in a 55-dimensional euclidean space
        ArrayList<Vector> P = readVectorsSeq(args[0]);

        // initialize the vector of weights as specified
        // in the assignment, i.e. an array as long as
        // the one of the points filled with ones
        ArrayList<Long> WP = new ArrayList<>(Collections.nCopies(P.size(), 1L));

        // start the elapsed time computation
        long start = System.currentTimeMillis();

        // compute the centers using the kmeansPP method
        ArrayList<Vector> C = kmeansPP(P, WP, k, iter);

        // compute the value of kmedianObj() and print it
        System.out.println("\nAverage distance between a point and its closest center (kmedianObj()): "
                + kmedianObj(P, C));

        // print the elapsed time
        System.out.println("Total elapsed time during clustering: " + (System.currentTimeMillis() - start) + " [ms]");
    }

    /*
     * Assigned methods
     */

    // this method receives as input a set of points represented
    // as an ArrayList of vectors (same as the output),
    // a list of weights represented as an ArrayList of longs,
    // the number of clusters k and the number of iterations as ints
    public static ArrayList<Vector> kmeansPP(ArrayList<Vector> P, ArrayList<Long> WP, int k, int iter) {

        if(P.size()<k)
            throw new IllegalArgumentException("Too Few Points");

        // initialize the random engine
        Random r = new Random();

        //initialize the container of the centers
        ArrayList<Vector> CPrime = new ArrayList<>();

        // initialize the centers' flag ArrayList
        ArrayList<Boolean> isCenter = new ArrayList<>(Collections.nCopies(P.size(), false));

        // initialize the vector of distances
        ArrayList<Double> distances = new ArrayList<>(Collections.nCopies(P.size(), Double.POSITIVE_INFINITY));

        // select the first center at random
        // and add it to the centers set
        int index = r.nextInt(P.size());
        CPrime.add(P.get(index));
        isCenter.set(index, true);

        // initialize the tuple container for the distances'
        // array and the cumulative sum
        Tuple2<ArrayList<Double>, ArrayList<Double>> updatedTuple;

        // initialize variables used inside the loop
        ArrayList<Double> cumSum;
        Iterator<Double> probabilitiesIterator;
        double sum, randNum;

        // log message
        //System.out.println("Starting kmeansPP() with K = " + k);

        // start the kmeans++ iterations
        for(int i = 1; i < k; i++) {
            // compute the distances using the
            // updateDistances method, to which
            // we pass the current distances array
            // the points and the newly added center
            updatedTuple = updateDistances(distances, P, WP, isCenter, CPrime.get(CPrime.size() - 1));

            // currDistances = updatedTuple._1;
            cumSum = updatedTuple._2;

            // draw the random uniform number
            randNum = r.nextDouble();

            // reset the index of the centroid we will add
            index = 0;

            // extract the total sum
            sum = cumSum.get(cumSum.size() - 1);

            // while the value of the probability is
            // less than the drawn threshold keep
            // increasing the index,
            // the variable sum is the total sum of all distances
            // therefore cumSum / sum belongs to [0, 1]
            probabilitiesIterator = cumSum.iterator();
            while(probabilitiesIterator.hasNext() && (probabilitiesIterator.next() / sum) < randNum) {
                index++;
            }

            // THESE CHECKS SHOULD NOT BE NEEDED,
            // WE ADDED THEM JUST FOR SAFETY
            // (as discussed during the appointment)

            // avoid to select a center that
            // has been already chosen
            while(index>-1 && isCenter.get(index)){
                index--;
            }
            //if we had a streak of centers all the way
            //from index to 0 we need to walk back to the first available
            //point and choose it as the center
            if( isCenter.get(0) && index < 0 )
                while(isCenter.get(++index));

            // add the center to the correct set
            CPrime.add(P.get(index));
            //set the corresponding flag to true
            isCenter.set(index, true);
        }

        // log messages
        //System.out.println("Starting refinement with iter = " + iter);

        // refine centers exploiting the Lloyd's algorithm
        CPrime = refineCenters(P, WP, CPrime, iter);

        // return the final centers
        return CPrime;
    }

    // this method refines the found centers
    // exploiting the Lloyd's algorithm
    private static ArrayList<Vector> refineCenters(ArrayList<Vector> P, ArrayList<Long> WP, ArrayList<Vector> C,
                                                   int iter) {
        // initialize variables used inside the loop
        ArrayList<Long> clusterSizes;
        ArrayList<Vector> newCenters;
        ArrayList<Integer> clusters;
        int currCluster;
        Vector currCenter;

        // execute the provided number of iterations
        for (int i = 1; i <= iter; i++) {
            // initialize a new array of zeros
            clusterSizes = new ArrayList<>(Collections.nCopies(C.size(), 0L));

            // nCopies does not work for this type of object
            newCenters = new ArrayList<>(C.size());
            for(int j = 0; j < C.size(); j++) {
                newCenters.add(Vectors.zeros(P.get(0).size()));
            }

            // do a partition
            clusters = partition(P, C);

            //reset the variables
            currCluster = -1;
            currCenter = null;

            // compute centroids
            for(int j = 0; j < P.size(); j++) {
                // get the points' cluster
                currCluster = clusters.get(j);

                // get the center
                currCenter = newCenters.get(currCluster);

                // add the point
                BLAS.axpy(WP.get(j), P.get(j), currCenter);

                // increment the cluster size count (weights are taken into account)
                clusterSizes.set(currCluster, clusterSizes.get(currCluster) + WP.get(j));
            }
            for(int j = 0; j < newCenters.size(); j++) {
                // get the object
                currCenter = newCenters.get(j);

                // normalize it by the cluster size
                if(clusterSizes.get(j)>0)
                    BLAS.scal(1.0 / clusterSizes.get(j), currCenter);
                else {
                    newCenters.set(j, C.get(j).copy());
                    //System.out.println("Broken Center: " + j + " Num Centers: " + newCenters.size() + " iter: " + i);
                }
            }
            C = newCenters;
        }

        // return the refined centers
        return C;
    }

    // this method computes the average distance between
    // a point and its closest center, returned as a double
    private static double kmedianObj(ArrayList<Vector> P, ArrayList<Vector> C) {
        // initialize the distances' cumulative sum
        double cumSum = 0;

        // initialize the double containing the min
        // and the one containing the considered distance
        double currMin, currDistance;

        // compute distances
        for(int i = 0; i < P.size(); i++) {
            // reset the minimum
            currMin = Double.POSITIVE_INFINITY;

            // check distances with all the centers
            for(int j = 0; j < C.size(); j++) {
                // compute the squared distance
                currDistance = Vectors.sqdist(P.get(i), C.get(j));

                // check for minimum
                if(currDistance < currMin) {
                    currMin = currDistance;
                }
            }

            // since the square root is a strictly
            // increasing function, inequalities over
            // the arguments have their direction preserved
            // therefore we only need to compute the square
            // root when we add the distance to the
            // cumulative sum and not during comparisons
            cumSum += Math.sqrt(currMin);
        }

        // return the average distance
        return cumSum / P.size();
    }

    /*
     * Auxiliary methods
     */

    private static Tuple2<ArrayList<Double>,ArrayList<Double>> updateDistances(ArrayList<Double> distances,
                                                                               ArrayList<Vector> P,
                                                                               ArrayList<Long> weights,
                                                                               ArrayList<Boolean> isCenter,
                                                                               Vector newCenter) {
        // initialize the temp variable for the distance
        // and the cumulative for the sum of all distances
        double currDistance, sum = 0;

        // initialize the cumulative array
        ArrayList<Double> cumulative = new ArrayList<>();

        // for each point, check if the new center is
        // closer than the previous closest
        for (int i = 0; i < P.size(); i++) {
            if (!isCenter.get(i)) {
                // compute the distance
                currDistance = Math.sqrt(Vectors.sqdist(P.get(i), newCenter));

                // check for minimum
                if (currDistance < distances.get(i)) {
                    //update the value
                    distances.set(i, currDistance);
                }

                // update the cumulative
                cumulative.add(sum += distances.get(i) * weights.get(i));
            }
            else {
                // update the cumulative with the previous value,
                // to preserve the strictly non-decreasing nature
                // of a cumulative function
                cumulative.add(sum);
            }
        }

        // return the lists grouped in a tuple
        return new Tuple2<>(distances, cumulative);
    }

    // this method partitions points among clusters
    private static ArrayList<Integer> partition(ArrayList<Vector> P, ArrayList<Vector> C) {
        // initialize the output List
        ArrayList<Integer> clusters = new ArrayList<>(P.size());

        // declare used variables
        double minDistance, currDistance;
        int minIndex;

        if(C.size()<1)
            System.out.println("Something went wrong, centers array length < 1");

        // partition
        for(int i = 0; i < P.size(); i++){
            // reset the minimum distance
            //and the index of the corresponding cluster
            minDistance = currDistance = Double.POSITIVE_INFINITY;
            minIndex = -1;

            // check and store the closest center
            for(int j = 0; j < C.size(); j++) {
                // compute the squared distance
                currDistance = Vectors.sqdist(P.get(i), C.get(j));


                // since the square root is a
                // monotonically increasing function,
                // the following check is still correct
                // and an additional operation is avoided
                // at each iteration
                if(currDistance < minDistance) {
                    minDistance = currDistance;
                    minIndex = j;
                }
            }
            clusters.add(minIndex);
        }

        // return the clusters
        return clusters;
    }

    /*
     * Methods provided by the VectorInput class
     */

    // this method parses a spark vector from a string
    private static Vector strToVector(String str) {
        String[] tokens = str.split(" ");
        double[] data = new double[tokens.length];
        for (int i = 0; i < tokens.length; i++) {
            data[i] = Double.parseDouble(tokens[i]);
        }
        return Vectors.dense(data);
    }

    // this method reads the entire file sequentially
    // and imports all of the vectors present inside
    private static ArrayList<Vector> readVectorsSeq(String filename) throws IOException {
        if (Files.isDirectory(Paths.get(filename))) {
            throw new IllegalArgumentException("readVectorsSeq is meant to read a single file.");
        }
        ArrayList<Vector> result = new ArrayList<>();
        Files.lines(Paths.get(filename))
                .map(Kmedian::strToVector)
                .forEach(result::add);
        return result;
    }

}